package org.academiadecodigo.rhashtafaris.mainmenu;

import org.academiadecodigo.simplegraphics.graphics.Color;
import org.academiadecodigo.simplegraphics.graphics.Rectangle;
import org.academiadecodigo.simplegraphics.keyboard.Keyboard;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardEvent;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardEventType;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardHandler;
import org.academiadecodigo.simplegraphics.pictures.Picture;

public class MainMenu implements KeyboardHandler {

    private final int PADDING = 10;
    private final int GRID_COLUMNS = 600;
    private final int GRID_ROWS = 450;

    private boolean space;
    private KeyboardEvent spacePressed;

    public MainMenu() {
        space = false;
        showGraphics();
        keyboardInitTitle();
    }

    private void showGraphics() {
        Rectangle background = new Rectangle(PADDING, PADDING, GRID_COLUMNS, GRID_ROWS);  //Creates black background
        background.setColor(Color.BLACK);
        background.fill();

        //Creates the left bar frame
        Rectangle frameLeft = new Rectangle (
                PADDING * 2,
                PADDING * 2,
                PADDING,
                GRID_ROWS - (PADDING * 2)
        );
        frameLeft.setColor(Color.RED);
        frameLeft.fill();

        //Creates the right bar frame
        Rectangle frameRight = new Rectangle(
                GRID_COLUMNS - PADDING,
                PADDING * 2,
                PADDING,
                GRID_ROWS - (PADDING * 2)
        );
        frameRight.setColor(Color.RED);
        frameRight.fill();

        //Creates the top bar frame
        Rectangle frameUp = new Rectangle(
                PADDING * 2,
                PADDING * 2,
                GRID_COLUMNS - (PADDING * 2),
                PADDING
        );
        frameUp.setColor(Color.RED);
        frameUp.fill();

        //Creates the down bar frame
        Rectangle frameDown = new Rectangle(
                PADDING * 2,
                GRID_ROWS - PADDING,
                GRID_COLUMNS - (PADDING * 2),
                PADDING
        );
        frameDown.setColor(Color.RED);
        frameDown.fill();

        Picture presentPicture = new Picture(115, 80, "resources/ecra_de_entrada_ilustrator_start.jpg"); //Loads and positions the title screen
        presentPicture.draw();
    }

    public void keyboardInitTitle() {
        Keyboard keyboard = new Keyboard(this);

        this.spacePressed = new KeyboardEvent();
        spacePressed.setKey(KeyboardEvent.KEY_SPACE);
        spacePressed.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);

        keyboard.addEventListener(spacePressed);
    }


    @Override
    public void keyPressed(KeyboardEvent keyboardEvent) {
        switch (keyboardEvent.getKey()) {
            case KeyboardEvent.KEY_SPACE:
                this.space = true;
                break;
        }
    }

    @Override
    public void keyReleased(KeyboardEvent keyboardEvent) {
    }

    public void spaceSelect() {
        while (!space) {
            try {
                Thread.sleep(50);

            } catch (InterruptedException ex) {
                System.err.println(ex.getMessage());
            }
        }
    }
}


