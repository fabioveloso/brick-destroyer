package org.academiadecodigo.rhashtafaris.mainmenu;

import org.academiadecodigo.bootcamp.Sound;
import org.academiadecodigo.simplegraphics.graphics.Color;
import org.academiadecodigo.simplegraphics.graphics.Rectangle;
import org.academiadecodigo.simplegraphics.keyboard.Keyboard;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardEvent;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardEventType;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardHandler;
import org.academiadecodigo.simplegraphics.pictures.Picture;

public class SelectGameMenu implements KeyboardHandler {

    private final int PADDING = 10;
    private final int GRID_COLUMNS = 600;
    private final int GRID_ROWS = 450;

    private Keyboard keyboard;
    private Rectangle background;
    private Rectangle frameLeft;
    private Rectangle frameRight;
    private Rectangle frameUp;
    private Rectangle frameDown;
    private Picture singleP;
    private Picture multiP;
    private boolean gameSelected = true;
    private boolean spaceP;


    public SelectGameMenu() {
        spaceP = false;
        showGraphics();
        KeyboardEvent[] keyboardEvents = keyboardInit();

        while (!spaceP) {
            try {
                Thread.sleep(50);

            } catch (InterruptedException ex) {
                System.err.println(ex.getMessage());
            }
        }
        spaceSelect(keyboardEvents);
    }

    public boolean isGameSelected() {
        return gameSelected;
    }

    private void showGraphics() {

        background = new Rectangle(PADDING, PADDING, GRID_COLUMNS, GRID_ROWS);  //Creates black background
        background.setColor(Color.BLACK);
        background.fill();

        frameLeft = new Rectangle(20, 20, 10, GRID_ROWS - 20);  //Creates the frame, left bar
        frameLeft.setColor(Color.RED);
        frameLeft.fill();

        frameRight = new Rectangle(((GRID_COLUMNS) / 2.0), 20, 10, GRID_ROWS - 20);  //Right bar
        frameRight.setColor(Color.RED);
        frameRight.fill();

        frameUp = new Rectangle(20, 20, ((GRID_COLUMNS - 20) / 2.0), 10);  //Up bar
        frameUp.setColor(Color.RED);
        frameUp.fill();

        frameDown = new Rectangle(20, GRID_ROWS - 10, ((GRID_COLUMNS - 20) / 2.0), 10);  //Down
        frameDown.setColor(Color.RED);
        frameDown.fill();

        singleP = new Picture(40, 80, "/resources/singleplayer1.jpg");
        multiP = new Picture(335, 80, "/resources/multiplayer_jogo.jpg");
        singleP.draw();
        multiP.draw();
    }

    private void hideGraphics() {
        background.delete();
        frameLeft.delete();
        frameRight.delete();
        frameUp.delete();
        frameDown.delete();
        singleP.delete();
        multiP.delete();
    }

    public KeyboardEvent[] keyboardInit() {

        this.keyboard = new Keyboard(this);

        KeyboardEvent rightPressed = new KeyboardEvent();
        rightPressed.setKey(KeyboardEvent.KEY_RIGHT);
        rightPressed.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);

        KeyboardEvent leftPressed = new KeyboardEvent();
        leftPressed.setKey(KeyboardEvent.KEY_LEFT);
        leftPressed.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);

        KeyboardEvent spacePressed = new KeyboardEvent();
        spacePressed.setKey(KeyboardEvent.KEY_SPACE);
        spacePressed.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);

        keyboard.addEventListener(rightPressed);
        keyboard.addEventListener(leftPressed);
        keyboard.addEventListener(spacePressed);

        return new KeyboardEvent[]{rightPressed, leftPressed, spacePressed};
    }

    @Override
    public void keyPressed(KeyboardEvent keyboardEvent) {

        switch (keyboardEvent.getKey()) {

            case KeyboardEvent.KEY_RIGHT:
                if(gameSelected){
                    moveRight();
                }
                gameSelected = false;
                break;
            case KeyboardEvent.KEY_LEFT:
                if(!gameSelected){
                    moveLeft();
                }
                gameSelected = true;
                break;
            case KeyboardEvent.KEY_SPACE:
                setSpaceP();
                break;
        }
    }

    @Override
    public void keyReleased(KeyboardEvent keyboardEvent) {
    }

    public void setSpaceP() {
        spaceP = true;
    }

    public void moveRight() {

        frameLeft.translate(290, 0);
        frameRight.translate(290, 0);
        frameUp.translate(290, 0);
        frameDown.translate(290, 0);
    }

    public void moveLeft() {

        frameLeft.translate(-290, 0);
        frameRight.translate(-290, 0);
        frameUp.translate(-290, 0);
        frameDown.translate(-290, 0);
    }

    public void spaceSelect(KeyboardEvent[] keyboardEvents) {
        if (gameSelected) {
            hideGraphics();

            for (KeyboardEvent keyBoardEvent: keyboardEvents) {
                this.keyboard.removeEventListener(keyBoardEvent);
            }
        }
    }
}



