package org.academiadecodigo.rhashtafaris;

import org.academiadecodigo.bootcamp.Sound;
import org.academiadecodigo.rhashtafaris.brickdestroyer.BrickDestroyerGame;
import org.academiadecodigo.rhashtafaris.mainmenu.MainMenu;
import org.academiadecodigo.rhashtafaris.mainmenu.SelectGameMenu;
import org.academiadecodigo.rhashtafaris.pongtobreak.PongGame;

public class Game {
    private Sound music;
    private boolean isPlaying;

    Game () {
        isPlaying = true;
        play();
    }

    private void play(){
        this.music = new Sound("/resources/Katyusha.wav");
        music.play(true);

        new MainMenu().spaceSelect();

        while (isPlaying){

            boolean playBricks = new SelectGameMenu().isGameSelected();
            this.music.stop();

            try {

                if(playBricks){
                    BrickDestroyerGame brickDestroyerGame = new BrickDestroyerGame();
                    brickDestroyerGame.init();
                    brickDestroyerGame.play();

                } else {
                    PongGame pongGame = new PongGame();
                    pongGame.init();
                    pongGame.play();
                }


            } catch (InterruptedException ex) {
                System.out.println(ex);
            }
        }
    }

}
